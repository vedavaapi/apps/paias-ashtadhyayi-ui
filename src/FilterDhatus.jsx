import { script } from "./App"
import MySelectSearch from "./MySelectSearch"

export default function Preferences() {
    const scriptOptions = [
        { value: "DEVANAGARI", label: "DEVANAGARI" },
        { value: "SLP1", label: "SLP1" },
    ]
    return (
        <>
            <div className="card m-3">
                <h5 className="card-header">Preferences</h5>
                <div className="card-body">
                    <MySelectSearch selectOptions={scriptOptions} onChange={(obj) => script.value = obj?.value || "DEVANAGARI"}></MySelectSearch>
                </div>
            </div>

        </>
    )
}