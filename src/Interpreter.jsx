import { useSignalEffect, useSignals } from "@preact/signals-react/runtime"
import OutputTable from "./OutputTable"
import { Input, TextField } from "@mui/material"
import { Loading, fetchResult, inpadas } from "./App"

export default function Interpreter() {
    useSignals()

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Interpret Input</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-12 col-md-3">
                                <TextField placeholder="Enter Input Here" variant="outlined" sx={{ mt: 2, maxWidth: 350 }} onChange={(e) => { inpadas.value = e.target.value; console.log(inpadas.value); }} />
                            </div>
                            <div className="col col-md-2 mt-4">
                                <button className="btn btn-primary" onClick={() => { fetchResult({ option: "Interpreter" }) }}>Run</button>
                                <Loading />
                            </div>
                        </div>
                        <div className="row">
                            <OutputTable />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}