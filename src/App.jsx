import { signal } from '@preact/signals-react';
import { useSignals } from '@preact/signals-react/runtime';

import Header from './Header';
import Preferences from './Preferences';

import KrdantaGenerator from './KrdantaGenerator';
import KrdantaAnalyser from './KrdantaAnalyser';
import PreValidated from './PreValidated';
import Interpreter from './Interpreter';
import SutraTab from './SutraTab';
import SamjnaTab from './SamjnaTab';

import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import { getQuery } from './QueryGenerator';


export const script = signal("DEVANAGARI")
export const filterGana = signal("")
export const filterAnta = signal("")
export const filterUpadha = signal("")

export const dhatus = signal([])
export const selectedDhatu = signal("")

export const pratyayas = signal([])
export const selectedPratyaya = signal("")

export const krdantas_for_analysis = signal([])
export const selectedKrdanta = signal("")

export const prevalidatedKrdantas = signal([])
export const selectedPrevalidated = signal("")

export const inpadas = signal("")
export const sutraInput = signal("")
export const samjnaSelected = signal("")

export const activeTab = signal("KrdantaGenerator")
export const outputJSON = signal()
export const loading = signal()

export function Loading() {
  useSignals()
  if (!loading.value) return "";
  return (
    <div className="spinner-border ms-2" role="status">
      <span className="visually-hidden">Loading...</span>
    </div>
  )
}

export function fetchResult({ option, ...params }) {
  loading.value = true
  const queryURL = getQuery({ option: option, params: params })
  fetch(queryURL)
    .then(res => res.json())
    .then(res => {
      outputJSON.value = res;
      loading.value = false
    })
}


export default function App() {
  useSignals()
  let body;
  if (activeTab.value == "KrdantaGenerator") {
    body = <KrdantaGenerator />
  }
  else if (activeTab.value == "KrdantaAnalyser") {
    body = <KrdantaAnalyser />
  }
  else if (activeTab.value == "PreValidated") {
    body = <PreValidated />
  }
  else if (activeTab.value == "Interpreter") {
    body = <Interpreter />
  }
  else if (activeTab.value == "SutraTab") {
    body = <SutraTab />
  }
  else if (activeTab.value == "SamjnaTab") {
    body = <SamjnaTab />
  }
  return (
    <>
      <div className="container-fluid m-3">
        <div className='row me-3'>
          <header className=""><Header /></header>
        </div>
        <div className='row me-3'>
          <div className="col-12 col-sm-9">
            {body}
          </div>
          <div className="col col-sm-3">
            <Preferences />
          </div>
        </div>
      </div>
    </>
  )

}