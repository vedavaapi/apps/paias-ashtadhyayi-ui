import { useSignalEffect, useSignals } from "@preact/signals-react/runtime";
import { Loading, fetchResult, krdantas_for_analysis, script, selectedKrdanta } from "./App";
import MySelectSearch from "./MySelectSearch";
import { apiUrl } from "./Settings";
import OutputTable from "./OutputTable";

export default function KrdantaAnalyser() {
    useSignals()
    useSignalEffect(() => {
        if (!script.value) return;

        // Load Krdantas_for_analysis
        fetch(apiUrl + `info/krdantas?show=krdanta_padas&outscript=${script.value}`)
            .then(res => res.json())
            .then(res => {
                let data = [];
                for (let i = 0; i < res['result'].length; i++) {
                    data.push({ label: res['result'][i], value: res['result'][i] })
                }
                krdantas_for_analysis.value = data
            })
    })

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Analyse Krdanta Forms</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-12 col-md-4">
                                <MySelectSearch placeholder={"Select Krdanta"} selectOptions={krdantas_for_analysis.value} onChange={(e, val) => selectedKrdanta.value = val}></MySelectSearch>
                            </div>
                            <div className="col-12 col-md-3 mt-4">
                                <button className="btn btn-primary" onClick={() => { fetchResult({ option: "KrdantaAnalyser" }) }}>Search</button>
                                <Loading />
                            </div>
                        </div>
                        <div className="row">
                            <OutputTable />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}