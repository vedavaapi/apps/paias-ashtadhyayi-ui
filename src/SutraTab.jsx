import { useSignals } from "@preact/signals-react/runtime"
import { TextField } from "@mui/material"
import { Loading, fetchResult, outputJSON, sutraInput } from "./App"
import ReactJson from "@microlink/react-json-view"

export default function SutraTab() {
    useSignals()

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Explore Sutra</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-12 col-md-3">
                                <TextField placeholder="Enter Sutra Number Here" variant="outlined" sx={{ mt: 2, maxWidth: 350 }} onChange={(e) => { sutraInput.value = e.target.value }} />
                                <p className="text-secondary">Eg. 13009</p>
                            </div>
                            <div className="col col-md-2 mt-4">
                                <button className="btn btn-primary" onClick={() => { fetchResult({ option: "SutraTab" }) }}>Search</button>
                                <Loading />
                            </div>
                        </div>
                        <div className="row mt-3">
                            {outputJSON.value ? <ReactJson src={outputJSON.value['result']} name="result" displayDataTypes={false} displayArrayKey={false} /> : ""}

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}