import { useSignalEffect, useSignals } from "@preact/signals-react/runtime"
import { activeTab, outputJSON } from "./App"

export default function Header() {
    useSignals()
    useSignalEffect(() => {
        // this console log is necessary to trigger re-render on change of activeTab value
        console.log("Tab Change", activeTab.value)
        outputJSON.value = "";
    })

    return (
        <>
            <p className="fs-2 fw-bold">
                PAIAS:&nbsp;
                <span style={{ color: 'var(--dark-blue)' }}>Ashtadhyayi Interpreter</span>
            </p>
            <nav className="navbar navbar-expand-lg">
                <ul className="nav nav-pills">
                    <li className="nav-item ms-2">
                        {
                            (activeTab.value == "KrdantaGenerator") ?
                                <a className="nav-link active" aria-current="page" href="#" onClick={() => { activeTab.value = "KrdantaGenerator" }}>Krdanta Generator</a>
                                :
                                <a className="nav-link" aria-current="page" href="#" onClick={() => { activeTab.value = "KrdantaGenerator" }}>Krdanta Generator</a>
                        }
                    </li>
                    <li className="nav-item">
                        {
                            (activeTab.value == "KrdantaAnalyser") ?
                                <a className="nav-link active" href="#" onClick={() => { activeTab.value = "KrdantaAnalyser" }}>Krdanta Analyser</a>
                                :
                                <a className="nav-link" href="#" onClick={() => { activeTab.value = "KrdantaAnalyser" }}>Krdanta Analyser</a>
                        }
                    </li>
                    <li className="nav-item">
                        {
                            (activeTab.value == "PreValidated") ?
                                <a className="nav-link active" href="#" onClick={() => { activeTab.value = "PreValidated" }}>Pre-validated Krdanta forms</a>
                                :
                                <a className="nav-link" href="#" onClick={() => { activeTab.value = "PreValidated" }}>Pre-validated Krdanta forms</a>
                        }
                    </li>
                    <li className="nav-item">
                        {
                            (activeTab.value == "Interpreter") ?
                                <a className="nav-link active" href="#" onClick={() => { activeTab.value = "Interpreter" }}>Interpreter</a>
                                :
                                <a className="nav-link" href="#" onClick={() => { activeTab.value = "Interpreter" }}>Interpreter</a>
                        }
                    </li>
                    <li className="nav-item">
                        {
                            (activeTab.value == "SutraTab") ?
                                <a className="nav-link active" href="#" onClick={() => { activeTab.value = "SutraTab" }}>Sutra</a>
                                :
                                <a className="nav-link" href="#" onClick={() => { activeTab.value = "SutraTab" }}>Sutra</a>
                        }
                    </li>
                    <li className="nav-item">
                        {
                            (activeTab.value == "SamjnaTab") ?
                                <a className="nav-link active" href="#" onClick={() => { activeTab.value = "SamjnaTab" }}>Samjna</a>
                                :
                                <a className="nav-link" href="#" onClick={() => { activeTab.value = "SamjnaTab" }}>Samjna</a>
                        }
                    </li>
                </ul>
            </nav>
        </>
    )
}