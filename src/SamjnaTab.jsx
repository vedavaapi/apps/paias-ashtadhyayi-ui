import { signal, useSignalEffect } from "@preact/signals-react";
import { Loading, fetchResult, outputJSON, samjnaSelected, script } from "./App";
import { useSignals } from "@preact/signals-react/runtime";
import { getQuery } from "./QueryGenerator";
import ReactJson from "@microlink/react-json-view";
import MySelectSearch from "./MySelectSearch";

const samjnaOptions = signal([])

export default function SamjnaTab() {
    useSignals()

    useSignalEffect(() => {
        if (!script.value) return;

        const url = getQuery({ option: "ListSamjnas" })
        fetch(url)
            .then(res => res.json())
            .then(res => {
                let data = [];
                for (let i = 0; i < res['result'].length; i++) {
                    data.push({ label: res['result'][i], value: res['result'][i] })
                }
                samjnaOptions.value = data
            })
    })

    useSignalEffect(() => {
        if (!samjnaSelected.value) return "";
        fetchResult({ option: "SamjnaTab" })
    })

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Samjnas</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-12 col-md-4">
                                <MySelectSearch placeholder={"Select Samjna"} selectOptions={samjnaOptions.value} onChange={(e, val) => samjnaSelected.value = val}></MySelectSearch>
                            </div>
                            <div className="col-12 col-md-3 mt-4">
                                <Loading />
                            </div>
                        </div>
                        <div className="row mt-3">
                            {samjnaSelected.value ? <ReactJson src={outputJSON.value['result']} name="result" displayDataTypes={false} displayArrayKey={false} /> : ""}
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}