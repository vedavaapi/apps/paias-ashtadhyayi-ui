import { Autocomplete, TextField } from "@mui/material";
import Select, { createFilter } from "react-select";

// eslint-disable-next-line react/prop-types
export default function MySelectSearch({ selectOptions, defaultValue, placeholder, onChange, width }) {

  return (<Autocomplete
    disablePortal
    autoHighlight
    options={selectOptions}
    value={defaultValue}
    getOptionLabel={(option) => option.label}
    getOptionKey={option => option.id}
    sx={{ maxWidth: width ?? 250, mt: 2 }}
    renderInput={(params) => <TextField {...params} label={placeholder} />}
    onInputChange={onChange}
    isOptionEqualToValue={(option, value) => option.id === value.id}
  />)

  // eslint-disable-next-line no-unreachable
  return (
    <>
      <Select
        filterOption={createFilter({ ignoreAccents: false })}
        className="basic-single"
        classNamePrefix="select"
        defaultValue={selectOptions[0]}
        isDisabled={false}
        isLoading={false}
        isClearable={true}
        isRtl={false}
        isSearchable={true}
        name="selectSearch"
        options={selectOptions}
        onChange={onChange}
        placeholder={placeholder}
      />
    </>
  );
}