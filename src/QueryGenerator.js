import { inpadas, samjnaSelected, script, selectedDhatu, selectedKrdanta, selectedPratyaya, sutraInput } from "./App";
import { apiUrl } from "./Settings";

export function getQuery({ option, ...params }) {
    // option is string
    // params is an object type
    let url = new URL(apiUrl)
    url.searchParams.set("logging_level", 0)
    url.searchParams.set("inscript", script.value)
    url.searchParams.set("outscript", script.value)

    switch (option) {
        case "KrdantaGenerator":
            // https://paias-api.sambhasha.ksu.ac.in/krtgen?dhatu_label=BU%20|%20gaRa=BvAdi%20|%20arTa=sattA/yAm&pratyaya=GaY&inscript=SLP1&outscript=SLP1&logging_level=0
            url.pathname = "krtgen"
            url.searchParams.set("dhatu_label", selectedDhatu.value)
            url.searchParams.set("pratyaya", selectedPratyaya.value)
            break;

        case "KrdantaAnalyser":
            url.pathname = "derive"
            url.searchParams.set("krdanta", selectedKrdanta.value)
            break;

        case "FetchPrevalidated":
            // https://paias-api.sambhasha.ksu.ac.in/info/krdantas?outscript=DEVANAGARI
            url.pathname = "info/krdantas"
            break;

        case "PreValidated":
            // https://paias-api.sambhasha.ksu.ac.in/krtgen?dhatu_label=ऋधुँ | गण=दिवादि | अर्थ=वृद्धौ/&pratyaya=क्यप्&inscript=DEVANAGARI&outscript=DEVANAGARI&logging_level=0
            url.pathname = "/krtgen"
            url.searchParams.set("dhatu_label", selectedDhatu.value)
            url.searchParams.set("pratyaya", selectedPratyaya.value)
            break;

        case "Interpreter":
            url.pathname = "/interpret"
            url.searchParams.set("inpadas", inpadas.value)
            break;

        case "SutraTab":
            url.pathname = "/info/sutra/" + sutraInput.value;
            break;

        case "SamjnaTab":
            url.pathname = "/info/samjna/" + samjnaSelected.value;
            break;

        case "ListSamjnas":
            url.pathname = "/info/samjnas";
            break;

        default:
            break;
    }
    console.log("getQuery returns ", url)
    return url
}