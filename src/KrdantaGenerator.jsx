import { useSignalEffect } from "@preact/signals-react";
import { useSignals } from "@preact/signals-react/runtime";
import MySelectSearch from "./MySelectSearch";
import { apiUrl } from './Settings';
import { Loading, dhatus, fetchResult, filterAnta, filterGana, filterUpadha, pratyayas, script, selectedDhatu, selectedPratyaya } from './App';
import OutputTable from "./OutputTable";

export default function KrdantaGenerator() {
    useSignals()
    useSignalEffect(() => {
        if (!script.value) return;

        // Load Dhatus
        const category = `gaRa:${filterGana.value},x_anta:${filterAnta.value},x_upadhA:${filterUpadha.value}`

        const url = new URL(apiUrl + `info/dhatus`)
        url.searchParams.set("inscript", script.value)
        url.searchParams.set("outscript", script.value)
        url.searchParams.set("category", category)

        fetch(url)
            .then(res => res.json())
            .then(res => {
                let data = [];
                for (let i = 0; i < res['result'].length; i++) {
                    data.push({ label: res['result'][i], id: i + 1 })
                    // data.push(res['result'][i]['dhatu_label'])
                }
                dhatus.value = data
            })
    })

    useSignalEffect(() => {
        // Load Pratyayas
        fetch(apiUrl + `info/krdantas?show=pratyayas&outscript=${script.value}`)
            .then(res => res.json())
            .then(res => {
                let data = [];
                for (let i = 0; i < res['result'].length; i++) {
                    data.push({ label: res['result'][i], id: i + 1 })
                    // data.push(res['result'][i])
                }
                pratyayas.value = data
            })
    })

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Generate Krdanta Forms</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-sm">
                                <MySelectSearch width={300} placeholder={"Select Dhaatu"} selectOptions={dhatus.value} onChange={(e, val) => selectedDhatu.value = val}></MySelectSearch>
                            </div>
                            <div className="col-sm">
                                <MySelectSearch width={300} placeholder={"Select Pratyaya"} selectOptions={pratyayas.value} onChange={(e, val) => selectedPratyaya.value = val}></MySelectSearch>
                            </div>
                            <div className="col mt-4">
                                <button className="btn btn-primary" onClick={() => { fetchResult({ option: "KrdantaGenerator" }) }}>Search</button>
                                <Loading />
                            </div>
                        </div>
                        <div className="row">
                            <OutputTable />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}