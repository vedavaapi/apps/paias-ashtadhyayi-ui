import { useSignalEffect, useSignals } from "@preact/signals-react/runtime"
import { filterAnta, filterGana, filterUpadha, script } from "./App"
import MySelectSearch from "./MySelectSearch"
import { apiUrl } from "./Settings"
import { activeTab } from "./App"
import { signal } from "@preact/signals-react"
import { spacing } from '@mui/system';

const antaOptions = signal()
const upadhaOptions = signal()
const ganaOptions = signal()

export default function Preferences() {
    useSignals()
    const scriptOptions = [
        { label: "DEVANAGARI", id: "DEVANAGARI" },
        { label: "SLP1", id: "SLP1" }
    ]

    useSignalEffect(() => {
        const url = new URL(apiUrl)
        url.pathname = "info/krdantas"
        url.searchParams.set("inscript", script.value)
        url.searchParams.set("outscript", script.value)

        url.searchParams.set("show", "dhatu_ganas")
        fetch(url).then(res => res.json()).then(res => {
            let data = []
            for (let i = 0; i < res['result'].length; i++) {
                data.push({ label: res['result'][i] })
            }
            ganaOptions.value = data
        })

        url.searchParams.set("show", "dhatu_anta")
        fetch(url).then(res => res.json()).then(res => {
            let data = []
            for (let i = 0; i < res['result'].length; i++) {
                data.push({ label: res['result'][i] })
            }
            antaOptions.value = data
        })

        url.searchParams.set("show", "dhatu_upadha")
        fetch(url).then(res => res.json()).then(res => {
            let data = []
            for (let i = 0; i < res['result'].length; i++) {
                data.push({ label: res['result'][i] })
            }
            upadhaOptions.value = data
        })
    })

    return (
        <>
            <div className="card mt-3">
                <h5 className="card-header">Preferences</h5>
                <div className="card-body">
                    <MySelectSearch placeholder={"Script"} selectOptions={scriptOptions} onChange={(e, val) => script.value = val} />
                </div>
            </div>
            {activeTab.value == "KrdantaGenerator" ?
                <div className="card mt-3">
                    <h5 className="card-header">Dhatu Filter</h5>
                    <div className="card-body">
                        <MySelectSearch sx={{}} placeholder={"Select gana"} selectOptions={ganaOptions.value} onChange={(e, val) => filterGana.value = val} />
                        <MySelectSearch sx={{}} placeholder={"Select anta"} selectOptions={antaOptions.value} onChange={(e, val) => filterAnta.value = val} />
                        <MySelectSearch sx={{}} placeholder={"Select upadha"} selectOptions={upadhaOptions.value} onChange={(e, val) => filterUpadha.value = val} />
                    </div>
                </div>
                : ""}
        </>
    )
}