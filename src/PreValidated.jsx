import { useSignalEffect, useSignals } from "@preact/signals-react/runtime"
import { Loading, fetchResult, prevalidatedKrdantas, script, selectedDhatu, selectedPratyaya, selectedPrevalidated } from "./App"
import MySelectSearch from "./MySelectSearch"
import OutputTable from "./OutputTable"
import { getQuery } from "./QueryGenerator"

export default function PreValidated() {
    useSignals()
    useSignalEffect(() => {
        if (!script.value) return;

        // Load Prevalidated
        fetch(getQuery({ option: "FetchPrevalidated" }))
            .then(res => res.json())
            .then(res => {
                let data = [];
                for (let i = 0; i < res['result'].length; i++) {
                    data.push({ label: res['result'][i]['dhatu_label'] + '+' + res['result'][i]['pratyaya'], id: i + 1 })
                    // data.push(res['result'][i]['dhatu_label'])
                }
                prevalidatedKrdantas.value = data
            })
    })

    return (
        <>
            <div className="card mt-3">
                <div className="card-header">
                    <h5>Validated Krdanta Forms</h5>
                </div>
                <div className="card-body">
                    <div className="container mt-1">
                        <div className="row">
                            <div className="col-sm">
                                <MySelectSearch width={400} placeholder={"Select Dhaatu+Pratyaya"} selectOptions={prevalidatedKrdantas.value} onChange={(e, val) => { selectedPrevalidated.value = val;[selectedDhatu.value, selectedPratyaya.value] = selectedPrevalidated.value.split('+') }}></MySelectSearch>
                            </div>
                            <div className="col mt-4">
                                <button className="btn btn-primary" onClick={() => { fetchResult({ option: "PreValidated" }) }}>Search</button>
                                <Loading />
                            </div>
                        </div>
                        <div className="row">
                            <OutputTable />
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}