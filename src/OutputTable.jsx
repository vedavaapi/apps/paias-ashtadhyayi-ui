import { useSignals } from "@preact/signals-react/runtime";
import { outputJSON } from "./App";
import { Fragment } from "react";
import { IconButton } from "@mui/material";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import ReactJson from "@microlink/react-json-view";

export default function OutputTable() {
    useSignals()
    if (!outputJSON.value) return ""
    return (
        <div className="mt-3">
            {outputJSON.value['generated'] ? <p className="fw-bold">Generated: {outputJSON.value['generated']} </p> : ""}
            {outputJSON.value['expected'] ? <p className="fw-bold">Expected: {outputJSON.value['expected']} </p> : ""}
            <table className="table table-striped mt-3">
                <thead className="table-dark">
                    <tr>
                        <th scope="col">Sutra Id</th>
                        <th scope="col">Sutra text</th>
                        <th scope="col">Sabdas</th>
                        <th scope="col">Output form</th>
                    </tr>
                </thead>
                <tbody>
                    {outputJSON.value['mods']?.map((row, idx) => {
                        return (
                            <Fragment key={idx}>
                                <tr>
                                    <td>{row['sutra_id']}</td>
                                    <td>{row['sutra_text']}</td>
                                    <td>{row['output'].split('-')[0].trim()}</td>
                                    <td>{row['output'].split('-')[1].trim()}</td>
                                </tr>
                                <tr className="mb-2">
                                    <td colSpan={4}>
                                        <IconButton data-bs-toggle="collapse" data-bs-target={"#collapse" + idx} aria-expanded="true" aria-controls={"collapse" + idx}>
                                            <KeyboardArrowDownIcon />
                                        </IconButton>
                                        <div id={"collapse" + idx} className="collapse show" style={{ color: '#059669' }}>
                                            <div className="accordion-body">
                                                <p>mahavakya: {row['sutra_mahavakya']}</p>
                                                <ReactJson src={row['trace']} name="trace" displayDataTypes={false} displayArrayKey={false} />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </Fragment>
                        )
                    })}
                </tbody>
            </table>

        </div>
    )
}